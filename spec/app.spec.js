// Jasmine provides the describe method
// Argument 1 gives a short description to the tested feature
// Argument 2 is a function that executes its expectations

// it is used for expectations

var request = require('request')

const base_url = 'https://localhost:8089'

describe('Test app.js', () => {
    describe('GET /',() => {
        it('return a status code of 200', () => {
            request.get(base_url, (error, response, body) => {
                expect(response.statusCode).toBe(200);
                document();
            })
        })
    })
})

describe('Test app.js', () => {
    describe('GET /account',() => {
        it('return a status code of 200', () => {
            request.get(base_url, (error, response, body) => {
                expect(response.statusCode).toBe(200);
                document();
            })
        })
    })
})

describe('Test app.js', () => {
    describe('GET /about',() => {
        it('return a status code of 200', () => {
            request.get(base_url, (error, response, body) => {
                expect(response.statusCode).toBe(200);
                document();
            })
        })
    })
})

describe('Test app.js', () => {
    describe('GET /category',() => {
        it('return a status code of 200', () => {
            request.get(base_url, (error, response, body) => {
                expect(response.statusCode).toBe(200);
                document();
            })
        })
    })
})

describe('Test app.js', () => {
    describe('GET /xyz',() => {
        it('return a status code of 404', () => {
            request.get(base_url, (error, response, body) => {
                expect(response.statusCode).toBe(404);
                document();
            })
        })
    })
})

describe('Test app.js', () => {
    describe('GET /transaction',() => {
        it('return a status code of 200', () => {
            request.get(base_url, (error, response, body) => {
                expect(response.statusCode).toBe(200);
                document();
            })
        })
    })
})

describe('Test app.js', () => {
    describe('GET /user',() => {
        it('return a status code of 200', () => {
            request.get(base_url, (error, response, body) => {
                expect(response.statusCode).toBe(200);
                document();
            })
        })
    })
})