/** 
*  account model
*  Describes the characteristics of each attribute in a account resource.
*
* @author Nishanth Reddy Devi Reddy <s534640@nwmissouri.edu>
*
*/

// see <https://mongoosejs.com/> for more information
const mongoose = require('mongoose')

const accountSchema = new mongoose.Schema({
  _id: { type: Number, required: true },
  accounttype: { type: String, required: true, unique: true, default: 'savings/checking' },
  accountnumber: { type: String, required: false },
  date: { type: Date, required: true, default: Date.now() }
})

module.exports = mongoose.model('account', accountSchema)
