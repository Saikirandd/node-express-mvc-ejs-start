
/** 
*  Category model
*  Describes the characteristics of each attribute in a category resource.
*
* @author Millindar Reddy <s534638@nwmissouri.edu>
*
*/

// see <https://mongoosejs.com/> for more information
const mongoose = require('mongoose')

const CategorySchema = new mongoose.Schema({
  _id: { type: Number, required: true },
  category_id: { type: Number, required: true, unique: true, default: 000 },
  category_name: { type: String, required: false, default: 'name' },
  date: { type: Date, required: true, default: Date.now() }
})

module.exports = mongoose.model('Category', CategorySchema)